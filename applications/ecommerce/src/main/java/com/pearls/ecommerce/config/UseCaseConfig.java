package com.pearls.ecommerce.config;

import com.pearls.ecommerce.model.gateways.TestGateway;
import com.pearls.ecommerce.usecase.TestUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCaseConfig {


    @Bean
    public TestUseCase testUseCase(TestGateway gateway) {
        return new TestUseCase(gateway);
    }
}
