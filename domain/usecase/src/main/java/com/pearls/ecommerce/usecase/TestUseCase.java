package com.pearls.ecommerce.usecase;

import com.pearls.ecommerce.model.gateways.TestGateway;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class TestUseCase {
    private final TestGateway gateway;

    public Optional<String> findById(int id) {
        return gateway.findById(id);
    }
}
