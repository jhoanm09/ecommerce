package com.pearls.ecommerce.model.gateways;

import java.util.Optional;

public interface TestGateway {
    Optional<String> findById(int id);
}
