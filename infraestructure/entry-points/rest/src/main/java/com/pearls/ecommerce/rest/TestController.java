package com.pearls.ecommerce.rest;

import com.pearls.ecommerce.usecase.TestUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class TestController {

    private final TestUseCase testUseCase;

    @GetMapping
    public ResponseEntity<String> test() {
        return ResponseEntity.ok(testUseCase.findById(2).get());
    }
}
