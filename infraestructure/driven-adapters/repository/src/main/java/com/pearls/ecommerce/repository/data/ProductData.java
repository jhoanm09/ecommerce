package com.pearls.ecommerce.repository.data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ProductData {
    @Id
    private Integer id;
    private String name;
}
