package com.pearls.ecommerce.repository;

import com.pearls.ecommerce.repository.data.ProductData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestRepository extends JpaRepository<ProductData, Integer> {
}
