package com.pearls.ecommerce.repository;

import com.pearls.ecommerce.model.gateways.TestGateway;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class TestRepositoryAdapter implements TestGateway {

   private final TestRepository testRepository;

    @Override
    public Optional<String> findById(int id) {
        return Optional.of("Apple");
    }
}
